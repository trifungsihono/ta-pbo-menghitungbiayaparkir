package com.yoiji;

public class Kendaraan {
    private int jamMasuk;
    private int jamKeluar;
    private int lamaJam;
    private double harga;

    public int getJamMasuk() {
        return jamMasuk;
    }

    public int getJamKeluar() {
        return jamKeluar;
    }

    public int getLamaJam() {
        return lamaJam;
    }

    public double getHarga() {
        return harga;
    }

    void Mobil(int jamKeluar, int jamMasuk, int lamaJa, double harga){
        this.jamKeluar = jamKeluar;
        this.jamMasuk = jamMasuk;
        this.lamaJam = lamaJam;
        this.harga = harga;
    }
}
