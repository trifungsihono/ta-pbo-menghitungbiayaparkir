package com.yoiji;
import java.util.Scanner;
public class Mobil{
    Scanner input = new Scanner(System.in);
    private int jamMasuk;
    private int jamKeluar;
    private int lamaJam;
    private double harga;

    void Mobil(int jamKeluar, int jamMasuk, int lamaJam, double harga){
        this.jamKeluar = jamKeluar;
        this.jamMasuk = jamMasuk;
        this.lamaJam = lamaJam;
        this.harga = harga;
    }

    public String biaya(){
        System.out.print("Masukkan Jam Masuk \t: ");
        jamMasuk = input.nextInt();
        System.out.print("Masukkan Jam Keluar : ");
        jamKeluar = input.nextInt();
        lamaJam=jamKeluar-jamMasuk;
        System.out.println("Lama Parkir \t\t: "+lamaJam+"jam");
        if(lamaJam>=4){
            System.out.println("Biaya Parkir \t\t: "+5000+"/jam");
            harga=((lamaJam*5000)*0.95);
            System.out.println("Biaya Parkir \t\t: Rp."+harga);
        }else{
            System.out.println("Biaya Parkir \t\t: "+5000+"/jam");
            harga=(lamaJam*5000);
            System.out.println("Biaya Parkir \t\t: Rp."+harga);
        }

        return null;
    }

    public Scanner getInput() {
        return input;
    }

    public void setInput(Scanner input) {
        this.input = input;
    }

    public int getJamMasuk() {
        return jamMasuk;
    }

    public void setJamMasuk(int jamMasuk) {
        this.jamMasuk = jamMasuk;
    }

    public int getJamKeluar() {
        return jamKeluar;
    }

    public void setJamKeluar(int jamKeluar) {
        this.jamKeluar = jamKeluar;
    }

    public int getLamaJam() {
        return lamaJam;
    }

    public void setLamaJam(int lamaJam) {
        this.lamaJam = lamaJam;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

}
